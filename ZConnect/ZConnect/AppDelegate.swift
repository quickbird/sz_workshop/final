//
//  AppDelegate.swift
//  ZConnect
//
//  Created by Stefan Kofler on 12.02.18.
//  Copyright © 2018 Zeppelin GmbH. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if PasswordService.isAlreadyLoggedIn() {
            let browserViewController = storyboard.instantiateViewController(withIdentifier: "BrowserNavigationController")
            window?.rootViewController = browserViewController
        } else {
            let loginViewController = storyboard.instantiateInitialViewController()
            window?.rootViewController = loginViewController
        }

        return true
    }

}

