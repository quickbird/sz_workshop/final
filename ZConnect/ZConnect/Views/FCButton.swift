//
//  FCButton.swift
//  Fahrerclub
//
//  Created by Stefan Kofler on 09.02.18.
//  Copyright © 2018 Zeppelin GmbH. All rights reserved.
//

import UIKit

class FCButton: UIButton {

    var isLoading: Bool = false {
        didSet {
            if isLoading {
                activityIndicatorView.startAnimating()
                titleEdgeInsets = UIEdgeInsets(top: CGFloat.greatestFiniteMagnitude, left: 0, bottom: 0, right: 0)
            } else {
                activityIndicatorView.stopAnimating()
                titleEdgeInsets = UIEdgeInsets.zero
            }
        }
    }

    private let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .white)

    override init(frame: CGRect) {
        super.init(frame: frame)

        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        commonInit()
    }

    init() {
        super.init(frame: .zero)

        commonInit()
    }

    private func commonInit() {
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(activityIndicatorView)

        activityIndicatorView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        activityIndicatorView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        activityIndicatorView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        activityIndicatorView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }

}
