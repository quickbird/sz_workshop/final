//
//  FCTextField.swift
//  Fahrerclub
//
//  Created by Stefan Kofler on 08.02.18.
//  Copyright © 2018 Zeppelin GmbH. All rights reserved.
//

import UIKit

class FCTextField: UITextField {

    var sideInsets: CGFloat = 16.0

    override init(frame: CGRect) {
        super.init(frame: frame)

        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        commonInit()
    }

    private func commonInit() {
        self.borderStyle = .none
        self.backgroundColor = UIColor.white
        self.applyShadow(radius: 1, opacity: 0.2, offset: CGSize(width: 0, height: 1))
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        var frame = bounds
        frame.origin.x = sideInsets
        frame.size.width -= sideInsets * 2
        return frame
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }

    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)

        newSuperview?.round(withCornerRadius: 5)
    }

}
