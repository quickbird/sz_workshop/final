//
//  PasswordService.swift
//  ZConnect
//
//  Created by Stefan Kofler on 12.02.18.
//  Copyright © 2018 Zeppelin GmbH. All rights reserved.
//

import Foundation
import KeychainSwift

class PasswordService {

    static let keychain = KeychainSwift()
    private static let UsernameKey = "PasswordServiceUsername"

    static func isAlreadyLoggedIn() -> Bool {
        return UserDefaults.standard.string(forKey: PasswordService.UsernameKey) != nil
    }

    static func getCredentials() -> (username: String, password: String) {
        let username = UserDefaults.standard.string(forKey: PasswordService.UsernameKey) ?? ""
        let password = keychain.get(username) ?? ""
        return (username: username, password: password)
    }

    static func storeCredentials(username: String, password: String) {
        keychain.set(password, forKey: username)
        UserDefaults.standard.set(username, forKey: PasswordService.UsernameKey)
    }

    static func deleteCredentials() {
        let username = UserDefaults.standard.string(forKey: PasswordService.UsernameKey) ?? ""
        keychain.delete(username)
        UserDefaults.standard.set(nil, forKey: PasswordService.UsernameKey)
    }

}
