//
//  LoginViewController.swift
//  SZWorkshop
//
//  Created by Stefan Kofler on 08.06.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class LoginViewController: UIViewController {
    var viewModel: LoginViewModel!

    @IBOutlet private var emailField: UITextField!
    @IBOutlet private var passwordField: UITextField!
    @IBOutlet private var loginButton: UIButton!

    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        loginButton.rx.tap
            .withLatestFrom(viewModel.outputIsLoginButtonEnabled)
            .filter { $0 }
            .subscribe(onNext: { _ in
                let employeesViewController = EmployeesViewController()
                employeesViewController.viewModel = EmployeesViewModelImpl()
                let navigationController = UINavigationController(rootViewController: employeesViewController)
                self.present(navigationController, animated: true, completion: nil)
            })
            .disposed(by: disposeBag)

        // Input
        emailField.rx.text.orEmpty
            .bind(to: viewModel.inputEmailText)
            .disposed(by: disposeBag)

        passwordField.rx.text.orEmpty
            .bind(to: viewModel.inputPasswordText)
            .disposed(by: disposeBag)

        // Output
        viewModel.outputIsLoginButtonEnabled
            .bind(to: loginButton.rx.isEnabled)
            .disposed(by: disposeBag)

        viewModel.outputIsLoginButtonEnabled
            .map { $0 ? 1.0 : 0.5 }
            .bind(to: loginButton.rx.alpha)
            .disposed(by: disposeBag)
    }
}
