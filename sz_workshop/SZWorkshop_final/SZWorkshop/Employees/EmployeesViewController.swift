//
//  TableViewController.swift
//  Schultopf
//
//  Created by Stefan Kofler on 06.06.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import UIKit
import RxSwift

class EmployeesViewController: UIViewController {
    var viewModel: EmployeesViewModel!

    @IBOutlet private var searchBar: UISearchBar!
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var activityIndicatorView: UIActivityIndicatorView!

    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Employees"
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.tableFooterView = UIView()

        bindViewModel()
    }

    func bindViewModel() {
        searchBar.rx.text.orEmpty
            .bind(to: viewModel.input.filterText)
            .disposed(by: disposeBag)

        viewModel.output.isLoading
            .bind(to: activityIndicatorView.rx.isAnimating)
            .disposed(by: disposeBag)

        tableView.rx.modelSelected(String.self)
            .subscribe(onNext: { [unowned self] employee in
                let viewController = EmployeeDetailViewController()
                viewController.viewModel = self.viewModel.detailViewModel(for: employee)
                self.navigationController?.pushViewController(viewController, animated: true)
            })
            .disposed(by: disposeBag)

        viewModel.output.employees
            .bind(to: tableView.rx.items(cellIdentifier: "Cell", cellType: UITableViewCell.self)) { _, employee, cell in
                cell.textLabel?.text = employee
            }
            .disposed(by: disposeBag)
    }

}
