//
//  LoginViewModelImpl.swift
//  SZWorkshop
//
//  Created by Stefan Kofler on 09.06.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import Foundation
import RxSwift

class LoginViewModelImpl: LoginViewModel {

    // MARK: - Inputs
    // TODO: Add input subjects from email, password and button trigger

    // MARK: - Outputs

    // TODO: Add output observable for input validation (using the two other observables below)

    // TODO: Add output observable for validated email

    // TODO: Add output observable for validated password

}
